/*
  This is my code for the HiGrow board.
  It sends the values to a mqtt server,
  this mqtt publish will only happen if the value is changed.
*/

// Include DTH, wifi and MQTT library
#include "DHT.h"
#include <WiFi.h>
#include <PubSubClient.h>

/*
  To optimice this code for your use,
  edit the values below this line
*/

/***********************************************/

// Define the DTH and soil moist meter pin
const int DHTPIN = 22;
const int SOILPIN = 32;

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Define network info
// Replace with your network credentials
const char* ssid     = "Network_ssid";      //wifi id
const char* password = "Network_password";  //password
const char* mqtt_server = "192.168.0.xxx";  //mqtt broker ip
const int mqtt_port = 1883;                 //mqtt broker port
const char* mqtt_user = "Username";         //mqtt username
const char* mqtt_password = "Passowrd";     //mqtt passowrd
const bool mqtt_retained = true;            //make mqtt server retain messages if device goes offline

// Define which MQTT topics the data is published to 
#define humidity_topic "sensor/device/humidity" 
#define temperature_topic "sensor/device/temperature"
#define heat_topic "sensor/device/heatindex"
#define soil_topic "sensor/device/soil"

// Get debugging values to serial (true)
const bool serial_debug = true;

/*
  Don't edit below this line
  unless you know what you are doing
*/

/*********************************************/

// Initialize DHT sensor.
DHT dht(DHTPIN, DHTTYPE);
// Temporary variables
static char celsiusTemp[7];
static char fahrenheitTemp[7];
static char humidityTemp[7];

WiFiClient espClient;
PubSubClient client(espClient);

// Setup a wifi connection
void wifisetup() {
  // We start by connecting to a WiFi network
  if (serial_debug) {
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  }

  WiFi.begin(ssid, password);

  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED) {
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    delay(500);
    if (serial_debug) Serial.print(".");
  }
  if (serial_debug) { 
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

// Attempts to reconnect to MQTT if we're ever disconnected 
void reconnect() { 
 // Loop until we're reconnected 
 while (!client.connected()) {  
   if (serial_debug) Serial.print("Attempting MQTT connection..."); 
   // Attempt to connect 
   if (client.connect("StueBirkClient", mqtt_user, mqtt_password)) { 
     if (serial_debug) Serial.println("connected");  
   } else {
     if (serial_debug) {  
       Serial.print("failed, rc="); 
       Serial.print(client.state()); 
       Serial.println(" try again in 5 seconds"); 
     }
     // Wait 5 seconds before retrying 
     delay(5000); 
   } 
 } 
} 

void setup() {
  // initialize the DHT sensor
  dht.begin();


  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  if (serial_debug) {  
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  } 

  // Initialize wifi
  wifisetup();

  // Initialize MQTT connection
  client.setServer(mqtt_server, mqtt_port);
}

// Helps check whether any value change is significant enough to warrant a data push 
bool checkBound(float newValue, float prevValue, float maxDiff) { 
 return !isnan(newValue) && 
        (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff); 
} 
long lastMsg = 0; 
float temp = 0.0; 
float hum = 0.0;
float heat = 0.0;
float soil = 0.0;
float diff = 0.1; 
void loop() {
  // Make sure we stay connected
  if (!client.connected()) {
    reconnect();
  }

  // Let MQTT do it's thing 
  client.loop(); 

  // Check the temp and humidity once per second 
  long now = millis(); 
  if (now - lastMsg > 1000) { 
    lastMsg = now; 
    // Read the values from the sensor 
    float newTemp = dht.readTemperature(); 
    float newHum = dht.readHumidity();
    // Check if any reads failed and exit early (to try again).
    if (isnan(newTemp) || isnan(newHum)) {
      if (serial_debug) Serial.println("Failed to read from DHT sensor!");
      strcpy(celsiusTemp, "Failed");
      strcpy(fahrenheitTemp, "Failed");
      strcpy(humidityTemp, "Failed");
    } else {
      // Check whether the temperature has changed 
      if (checkBound(newTemp, temp, diff)) {
        temp = newTemp;
        if (serial_debug) {
          Serial.print("New temperature:"); 
          Serial.println(String(temp).c_str()); 
        }
        client.publish(temperature_topic, String(temp).c_str(), mqtt_retained);
      }

      // Check whether the humidity has changed 
      if (checkBound(newHum, hum, diff)) { 
        hum = newHum; 
        if (serial_debug) {
          Serial.print("New humidity:"); 
          Serial.println(String(hum).c_str()); 
        }
        client.publish(humidity_topic, String(hum).c_str(), mqtt_retained);
      }
      
      // Compute heat index in Celsius (isFahreheit = false)
      float newHeat = dht.computeHeatIndex(temp, hum, false);
      // Check whether the humidity has changed 
      if (checkBound(newHeat, heat, diff)) { 
        heat = newHeat; 
        if (serial_debug) {
          Serial.print("New heatindex:"); 
          Serial.println(String(heat).c_str()); 
        }
        client.publish(heat_topic, String(heat).c_str(), mqtt_retained);
      }
    }
    float newSoil = analogRead(32);
    if (checkBound(newSoil, soil, diff)) { 
      soil = newSoil; 
      if (serial_debug) {
        Serial.print("New waterlevel:"); 
        Serial.println(String(soil).c_str());
      }
      client.publish(soil_topic, String(soil).c_str(), mqtt_retained);
    }
  }
}
